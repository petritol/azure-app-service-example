# BEHOLD! The mighty Azure App Service + PostgreSQL example!

## How to run locally
- Run `npm install` to install dependencies
- Create `.env` and replace the values with your PostgreSQL information (see `.env.example`)
- Make sure that the database you defined has a table named `products`, bonus points if it has some rows in it (what columns it includes is up to you)
- Run `npm start`
- Navigate to http://localhost:3000

## How to run on Azure App Services
- Create a web app in Azure App Services (as instructed in the lecture)
- Set the same environmental variables as in `.env.example` in your web app's settings (Settings -> Configuration -> Application settings)
- Deploy this JS app to your web app
- Navigate to your app's address and marvel at how it prints the same contents as your local version did and what possibilities this allows