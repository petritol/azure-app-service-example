import express from 'express';
import pg from 'pg';
import 'dotenv/config';

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;
const pool = new pg.Pool({
  host: PG_HOST,
  port: Number(PG_PORT),
  user: PG_USERNAME,
  password: String(PG_PASSWORD),
  database: PG_DATABASE,
  ssl: true
});
const server = express();

server.get('/', async (_req, res) => {
  const client = await pool.connect();
  try {
    const result = await client.query("SELECT * from products");
    console.log(result.rows);
    res.send(result.rows);
  } catch (error) {
    console.error(error.stack);
    error.name = "dbError";
    throw error;
  } finally {
    client.release();
  }
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log('Server listening port', PORT);
});
